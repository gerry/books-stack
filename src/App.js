import React, { useState, useContext } from 'react';
import { BooksContext } from './context/books.context';
import Stack from './components/stack.component';
import './App.css';

function App() {
  const [book, setBook] = useState("");
  const context = useContext(BooksContext);
  const { addBook, removeBook } = context;

  const handleAddBookInput = (e) => {
    setBook(e.target.value)
  }

  const handleBookSubmit = () => {
    if(book !== "" && book !== undefined) {
      addBook(book);
    }
    setBook("");
  }

  return (
    <div className="App">
      <h1>Stack of Books</h1>
      <section>
        <input className='text-field' type={`text`} value={book} onChange={handleAddBookInput} />
        <button type="button" className='button' onClick={handleBookSubmit}>Add Book</button>
        <button type="button" className='button' onClick={removeBook}>Remove Book</button>
      </section>
      <Stack />
    </div>
  );
}

export default App;
