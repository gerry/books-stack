import React, { createContext, useState } from "react";

export const BooksContext = createContext({
  books: null,
  addBook: () => null,
  removeBook: () => null,
});

export const BooksProvider = ({ children }) => {
  const [books, setBooks] = useState([]);

  const addBook = (book) => {
    books.unshift(book);
    setBooks([...books]);
  }

  const removeBook = () => {
    books.shift();
    setBooks([...books]);
  }

  const value = { books, addBook, removeBook };

  return <BooksContext.Provider value={value}>{children}</BooksContext.Provider>
}