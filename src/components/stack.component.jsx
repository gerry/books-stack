import React, { useContext } from "react";
import { BooksContext } from "../context/books.context";

const Stack = () => {
  const { books } = useContext(BooksContext);

  return (
    <section>
      {
        books.map((book, index) => (
          <div key={index} className='book'>
            {book}
          </div>
        ))
      }
    </section>
  )
}

export default Stack;